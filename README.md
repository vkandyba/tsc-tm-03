# TASK MANAGER

SCREENSHOTS

https://disk.yandex.ru/d/q67vRxv0oTw1xg?w=1

# DEVELOPER INFO

name: Vyacheslav Kandyba

e-mail: vkandyba@tsconsulting.com

# HARDWARE

CPU: i7

RAM: 16GB

SSD: 512GB

# SOFTWARE

System: Windows 10

Version JDK: openjdk version "1.8.0_41"

# PROGRAM RUN

```
java -jar ./task-manager.jar
```
