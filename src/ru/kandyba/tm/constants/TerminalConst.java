package ru.kandyba.tm.constants;

public class TerminalConst {

    public static final String ABOUT = "about";

    public static final String VERSION = "version";

    public static final String HELP = "help";

}
